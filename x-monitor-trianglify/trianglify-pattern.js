/**
 * React represantation of two trianglify pattern
 */
class TrianglifyPattern extends React.Component {
  // Setting up the state
  constructor(props) {
    super(props);
    this.state = {
      pattern1: Trianglify({
        width: window.innerWidth,
        height: window.innerHeight
      }),
      pattern2: Trianglify({
        width: window.innerWidth,
        height: window.innerHeight
      })
    };
  }

  // Setting the first pattern as current on mount
  componentWillMount() {
    this.setState({
      current: this.state.pattern1
    });
  }

  // Setting up the timer to switch the patterns
  componentDidMount() {
    this.timerID = setInterval(
      () => this.switchCurrent(),
      60000
    );
  }

  // Clearing timer interval on removal
  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  // Responsible to switch the patterns
  switchCurrent() {
    // Toggling the current pattern
    this.setState({
      current: this.state.current === this.state.pattern1 ? this.state.pattern2 : this.state.pattern1,
    });

    // Recreating the whole pattern for the invisible element after transtion is finished
    setTimeout(()=>{
      this.setState({
        pattern1: this.state.current === this.state.pattern1 ? this.state.pattern1 : Trianglify({
          width: window.innerWidth,
          height: window.innerHeight
        }),
        pattern2: this.state.current === this.state.pattern2 ? this.state.pattern2 : Trianglify({
          width: window.innerWidth,
          height: window.innerHeight
        })
      });
    }, 6000)
  }

  render() {
    // Defining the shadow elements stylesheet
    const elementStyles = `
    @keyframes x-monitor-trianglify-zoomoutin {
      0% {
        transform: scale(1);
      }
      50% {
        transform: scale(1.5);
      }
      100%{
        transform: scale(1);
      }
    }

    div.background{
      transition: opacity 5s cubic-bezier(0.075, 0.82, 0.165, 1);
      position: absolute;
      opacity: 0;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      user-select: none;
    }

    div.zoom{
      animation: x-monitor-trianglify-zoomoutin 60s infinite ease-in-out;
    }

    div.background.visible{
      opacity: 1;
    }

    div.container{
      overflow:hidden;
      height: 100%;
      width: 100%;
      position: absolute;
    }
    `;

    // Creates the template for the patterns
    return (
      React.createElement('div', {className: "container"},
      React.createElement('style', null, elementStyles),
      React.createElement('div', {className: "background " + (this.state.current === this.state.pattern1 ? 'visible ' : '') + (this.props.zoom === true ? 'zoom ' : ''), 'style': {backgroundImage: "url('"+this.state.pattern1.png()+"')"}}),
      React.createElement('div', {className: "background " + (this.state.current === this.state.pattern2 ? 'visible ' : '') + (this.props.zoom === true ? 'zoom ' : ''), 'style': {backgroundImage: "url('"+this.state.pattern2.png()+"')"}})
    )
  );
}
}
