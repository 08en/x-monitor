/**
 * React represantation of two trianglify pattern
 */
class OpenWeather extends React.Component {
  // Setting up the state
  constructor(props) {
    super(props);
    this.state = {
      data: {
        main: {
          temp: 0
        }
      }
    };

    this.xmlhttp = new XMLHttpRequest();
  }

  componentWillReceiveProps(nextProps) {
    this.xmlhttp.abort();
    this.xmlhttp.open("GET", "http://api.openweathermap.org/data/2.5/weather?q="+this.props.city+"&units=metric&appid=543a21df6d2a7ea192863693bc74e645", true);
    this.xmlhttp.send();
  }

  // Setting the first pattern as current on mount
  componentWillMount() {
    this.xmlhttp.onreadystatechange = () => {
        if (this.xmlhttp.readyState == XMLHttpRequest.DONE ) {
           if (this.xmlhttp.status == 200) {
             this.setState({data: JSON.parse(this.xmlhttp.responseText)});
           }
        }
    };
  }

  render() {
    const weatherCode = () => {
      return this.state.data && this.state.data.weather && this.state.data.weather[0] ? this.state.data.weather[0].id : 0;
    }

    const temperature = () => {
      return this.state.data && this.state.data.main && this.state.data.main ? this.state.data.main.temp + " °C" : "";
    }

    const city = () => {
      return this.state.data && this.state.data.name ? this.state.data.name : "";
    }

    const createTemperatureElement = () => {
      return this.props.showTemperature ? React.createElement('div', {className: "temperature-text"}, temperature()) : "";
    }

    const createCityElement = () => {
      return this.props.showCity ? React.createElement('div', {className: "city-text"}, city()) : "";
    }

    // Creates the template for the patterns
    return (
      React.createElement('div', {className: "container"},
        React.createElement('i', {className: "owf owf-4x owf-" + weatherCode()}, null),
        createTemperatureElement(),
        createCityElement()
      )
    );
}
}
